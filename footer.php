
        <footer class="site-footer">
            <div class="footer-credits">
                <p class="footer-copyright">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
                     |  Design & Coding by :
                    <a class="copyright-link" href="<?php echo esc_url( __('https://ehsanghaffarii.ir/', 'EinTheme') );?>">
                        <?php _e( 'EhsanGhaffarii', 'EinTheme'); ?>
                    </a>
                     | &copy; 2020
                </p>
            </div>
        </footer>
           <?php wp_footer(); ?>
        
    </body>
</html>