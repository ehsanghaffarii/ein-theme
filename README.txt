=== Ein-Theme ===
Contributor: EhsanGhaffarii
Contributor URI: https://EhsanGhaffarii.ir
Requires at least: WordPress 4.7
Tested up to: WordPress 5.3
Requires PHP: 7.3.1
Version: 1.0 
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-column, two-columns, right-sidebar, flexible-header, accessibility-ready, custom-colors, custom-header, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, post-formats, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready


== Description ==
The first WordPress theme designed and coded by the Ein-Graphic group.
It uses Bootstrap 4.4 and jQuery 3.4.1 


=== install ===
01.download the zip file
02.in WordPress dashboard go to => Appearance > Themes > add new > upload theme 
03.upload the zip file
04.active the theme and enjoy

=== Copyright ===

Ein-Theme WordPress Theme, Copyright 2020 by Ein-Graphic
Ein-Theme is distributed under the terms of the GNU GPL.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

=== Social Network ===
instagram: https://instagram.com/EhsanGhaffarii
linkedin: https://linkedin.com/EhsanGhaffarii
