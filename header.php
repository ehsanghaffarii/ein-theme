<!doctype html>
<html <?php language_attributes(); ?>>
        <head>
            <meta charset="<?php bloginfo('charset'); ?>" />
            <title><?php wp_title() ; ?></title>
            <meta name="description" content="<?php bloginfo('description'); ?>">
            <?php wp_head(); ?>
        </head>
    <body <?php body_class(); ?>>

        <header id="header">
            <nav class="navbar navbar-expand-sm">
                <a class="navbar-brand" href="<?=site_url() ?>"> <?php esc_attr_e( 'EinTheme') ; ?></a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation"></button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            
                         <?php wp_list_pages( 'title_li' ); ?>
            
                    </ul>
                </div>
            </nav>
        </header>