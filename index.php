<?php get_header(); ?>

    <article class="content">
        <section class="posts">
            <?php if(have_posts()) : ?> 
                <?php while(have_posts()) : the_post(); ?>
                    <h2><a href="<? the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_content(); ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </section>    
    </article>

<?php get_footer(); ?>