
<?php


/* stylesheet function */
function custom_theme_assets() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'custom_theme_assets');


/* scripts function */
function custom_theme_scripts() {
    wp_enqueue_script('script', get_template_directory_uri(). 'js/jquery-3.4.1.js', array('jquery'), true);
}
add_action( 'wp_enqueue_scripts', 'custom_theme_scripts');



/* theme setup functions */
if( ! function_exists('eintheme_setup')) :
    function eintheme_setup() {

        /* post thumbnails */
        add_theme_support('post-thumbnails');

        /* add support for navs */
        register_nav_menus( array(
            'primary'  => __('Primary Menu', 'eintheme'),
            'secondary' => __('Secondary Menu', 'eintheme')
         ) );

         /* enable support for
         * aside, gallery, image & ... 
        */
        add_theme_support('post-format', array('aside', 'gallery', 'image', 'video'));
    }
endif;
add_action('after_setup_theme', 'eintheme_setup');

